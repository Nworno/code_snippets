import pandas as pd

nest_dic = {'key1': {'sub-key1': {'sub-sub-key1': ['a','b','c','d','e']}},
            'key2': {'sub-key2': {'sub-sub-key2': ['1','2','3','5','8','9','10']}}}

df = pd.DataFrame.from_dict({(i, j, k): nest_dic[i][j][k]
                            for i in nest_dic.keys()
                            for j in nest_dic[i].keys()
                            for k in nest_dic[i][j].keys()},
                            orient="index")
df.index = pd.MultiIndex.from_tuples(df.index)
print(df)

                            0  1  2  3  4     5     6
key1 sub-key1 sub-sub-key1  a  b  c  d  e  None  None
key2 sub-key2 sub-sub-key2  1  2  3  5  8     9    10
