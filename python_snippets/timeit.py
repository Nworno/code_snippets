
def dummy_function():
    lst = []
    for i in range(1000000):
        lst.append('x')
    print("done")
    return

if __name__ == '__main__':
    from timeit import timeit
    timeit(stmt="dummy_function()", globals=globals(), number=20)

