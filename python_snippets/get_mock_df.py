from functools import reduce
from typing import List, Tuple, Union

import pandas as pd
import numpy as np


def get_mock_df(n_rows: int = 100,
                int_cols: Union[None, int] = 10,
                float_cols: Union[None, int] = 10,
                strings_cols: Union[None, int] = 10,
                int_cols_range: Tuple[int] = (0, 10),
                float_cols_range: Tuple[int] = (0, 1),
                strings_cols_choice: Union[List[str], str] = "abcdefghijklmnopqrstuvwxyz",
                nan_proportion: Union[None, int] = 0.05,
                col_names: List[str] = None,
                index_names: List[str] = None) -> pd.DataFrame:
    """
    args:
    n_rows: int = 100,
    int_cols: Union[None, int] = 10,
    float_cols: Union[None, int] = 10,
    strings_cols: Union[None, int] = 10,
    int_cols_range: Tuple[int] = (0, 10),
    float_cols_range: Tuple[int] = (0, 1),
    strings_cols_choice: Union[List[str], str] = "abcdefghijklmnopqrstuvwxyz",
    nan_proportion: Union[None, int] = 0.05,
    col_names: List[str] = None,
    index_names: List[str] = None
    """
    list_arrays = []
    if int_cols not in [None, 0]:
        list_integers_cols = [np.random.randint(*int_cols_range, size=int_cols) for col in range(0, n_rows)]
        list_arrays.append(np.array(list_integers_cols))
    if float_cols not in [None, 0]:
        list_float_cols = [np.random.uniform(*float_cols_range, size=float_cols) for col in range(0, n_rows)]
        list_arrays.append(np.array(list_float_cols))
    if strings_cols not in [None, 0]:
        list_strings_cols = []
        for col in range(0, n_rows):
            if isinstance(strings_cols_choice, str):
                list_letters= [np.random.choice(list(strings_cols_choice), replace=True, size=8).tolist() for len_word in range(0, strings_cols)]
                list_words = [reduce(lambda a,b: a+b, word) for word in list_letters]
                list_strings_cols.append(list_words)
            else:
                list_words = np.random.choice(strings_cols_choice, replace=True, size=strings_cols)
                list_strings_cols.append(list_words)
        list_arrays.append(np.array(list_strings_cols))

    mock_array = np.hstack(list_arrays)
    mock_df = pd.DataFrame(mock_array)

    if col_names is not None:
        mock_df.columns = col_names
    if index_names is not None:
        mock_df.index = index_names

    if nan_proportion not in [None, 0]:
        tuple_indices = []
        n_rows, n_cols = mock_df.shape
        for row in range(0, n_rows):
            for col in range(0, n_cols):
                tuple_indices.append((row, col))
        to_replace_indices = np.random.choice(range(0, len(tuple_indices)),
                                              size=int(np.ceil(len(tuple_indices)*nan_proportion)),
                                              replace=False)
        for to_replace_indice in to_replace_indices:
            indice = tuple_indices[to_replace_indice]
            mock_df.iloc[indice[0], indice[1]] = np.NaN

    return mock_df


if __name__ == "__main__":
    import argparse #WIIIP
    #df = create_mock_df(n_rows=100: int,
    #               ncols_integers = 10: [None, int],
    #               int_cols_range = (0, 10): Tuple[int],
    #               ncols_float = 10: [None, int],
    #               float_cols_range = (0, 1): Tuple[int],
    #               ncols_strings = 10: [None, int],
    #               strings_cols_choice="abcdefghijklmnopqrstuvwxyz": str,
    #               nan_cols = [9, 19, 29]:[None, List[int]],
    #               colnames = None: List[str])
    #df.to_csv("./mock_df.csv")
#
